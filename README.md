### Multilingual Named entity recognition for medieval charters

This is the paper code version for the article : 

[AGUILAR, Sergio Torres. Multilingual Named Entity Recognition for Medieval Charters Using Stacked Embeddings and Bert-based Models. En Proceedings of the Second Workshop on Language Technologies for Historical and Ancient Languages. 2022. p. 119-128.](https://aclanthology.org/2022.lt4hala-1.17.pdf)



### Requeriments
* Tensorflow 2
* Pytorch >1.0.5
* Transformers >3.0.1
* Flair 0.9
* Seqeval >1.2.2
* Keras >2.3.1
* CuDA >10.1
* CuDNN >7.0


### Flair and Bert environments

1) Taggers for named entity recognition can be tested and trained using the annotated data in the Flair environments. The wget to the test, val and train datasets are already indicated in the notebook, you must follow the instructions in order to fit the data into the NER architectures. We provide two Colab notebooks ready to produce robust NER models for medieval french on the bi-lstm and Bert-based architectures:

2) Try the bi-lstm + stacked embeddings Architecture: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/11xGSF5QVLj9v16DasNc1qrTzT8UUK-NW?usp=sharing)


3) Try Roberta Architecture: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1EwwojyNo8LYYoytoeBa6veSWEksHoqdJ?usp=sharing) 

The original dataset, the monolingual and multilingual models are released in a Zenodo repository:
[![https://doi.org/10.5281/zenodo.6463699](https://zenodo.org/badge/doi/10.5281/zenodo.6463699.svg)](https://doi.org/10.5281/zenodo.6463699)


The best RoBERTa model cal also be tested and cloned in the HuggingFace library: [Open HuggingFace](https://huggingface.co/magistermilitum/roberta-multilingual-medieval-ner)


