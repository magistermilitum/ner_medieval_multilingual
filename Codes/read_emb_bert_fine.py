
from sklearn.model_selection import train_test_split
import codecs
import json
import pandas as pd


with open('all_multilingual_31_01_2022.json', encoding="utf-8") as f:
  txt_files = json.load(f)



train_sents, test_sents= train_test_split(txt_files, test_size=0.10, random_state=2018)
train_sents, val_sents= train_test_split(train_sents, test_size=0.07, random_state=2018)


with codecs.open('train_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in train_sents))

with codecs.open('val_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in val_sents))

with codecs.open('test_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in test_sents))




# define columns to construct our model on flair data
from flair.models import SequenceTagger
from flair.data import Sentence
from flair.data import Corpus
from flair.datasets import ColumnCorpus
#columns = {0 : 'text', 2 : 'ner'} #to train on PERS label
columns = {0 : 'text', 2 : 'ner'} #to train on LOC label

# directory where the data resides
data_folder = '' 

# initializing the corpus: you must load the training sets in your enviromment
corpus: Corpus = ColumnCorpus(data_folder, columns,
                              train_file = 'train_elec.txt',
                              test_file = 'test_elec.txt',
                              dev_file = 'val_elec.txt')



# tag to predict
tag_type = 'ner'
# make tag dictionary from the corpus
tag_dictionary = corpus.make_tag_dictionary(tag_type=tag_type)


print(tag_dictionary)


#EMBEDDINGS

from flair.embeddings import FlairEmbeddings, TransformerWordEmbeddings
from flair.embeddings import WordEmbeddings
from flair.data import Sentence
from flair.embeddings import WordEmbeddings, StackedEmbeddings, FlairEmbeddings

# 4. initialize fine-tuneable transformer embeddings WITH document context
from flair.embeddings import FlairEmbeddings, TransformerWordEmbeddings

embeddings = TransformerWordEmbeddings(model='xlm-roberta-large',
                                       layers="-1",
                                       subtoken_pooling="first",
                                       fine_tune=True,
                                       use_context=True,
                                       )



# 5. initialize bare-bones sequence tagger (no CRF, no RNN, no reprojection)
from flair.models import SequenceTagger
from flair.trainers import ModelTrainer
tagger = SequenceTagger(hidden_size=256,
                        embeddings=embeddings,
                        tag_dictionary=tag_dictionary,
                        tag_type='ner',
                        use_crf=False,
                        use_rnn=False,
                        reproject_embeddings=False,
                        )

# 6. initialize trainer
trainer = ModelTrainer(tagger, corpus)



# 7. run fine-tuning
trainer.fine_tune('resources/taggers/sota-ner-flert',
                  learning_rate=5.0e-6,
                  mini_batch_size=4,
                  mini_batch_chunk_size=1,
                  embeddings_storage_mode='cpu',# remove this parameter to speed up computation if you have a big GPU
                  )

