import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
import torch
from torch.utils.data import Dataset, DataLoader
from transformers import BertTokenizerFast, BertConfig, BertForTokenClassification


from torch import cuda
device = 'cuda' if cuda.is_available() else 'cpu'
print(device)

import json
import random
from sklearn.model_selection import train_test_split



def limit(dt, num=180):
    data=[]
    box=[]
    counter=0
    f = lambda A, n=100: [A[i:i+n] for i in range(0, len(A), n)]

    for i,item in enumerate(dt):
      for i_i, word in enumerate(item[:-1]):
        try:
          if word[0]=="." and item[i_i+1][0][0].isupper() and counter>12:
            data.extend(f(box+[word]))
            box=[]
            counter=0
          else:
            box.append(word)
            counter+=1
        except:
          continue

      if len(box)>0:
        if len(box)>num:
          data.extend(f(box))
          counter=0
        else:
          data.append(box)
          counter=0
      data[-1]=data[-1]+[item[-1]]
      box=[]
      counter=0
    return data




with open('all_multilingual_31_01_2022.json', encoding='utf-8') as fh:
    data_orig = json.load(fh)

data_orig=[[y for y in x if len(y[0])>0] for x in data_orig]

'''
shuffler=list(range(len(data_orig)))
random.shuffle(shuffler)

data_orig=[data_orig[_] for _ in shuffler]
'''



train_dataset, test_dataset= train_test_split(data_orig, test_size=0.10, random_state=2018)
train_dataset, dev_dataset =train_test_split(train_dataset, test_size=0.06, random_state=2018)


data=pd.DataFrame([[" ".join([y[0] for y in x]), ",".join([y[2] for y in x])] for x in limit(data_orig)], columns=["sentence", "word_labels"])
train_dataset=pd.DataFrame([[" ".join([y[0] for y in x]), ",".join([y[2] for y in x])] for x in limit(train_dataset)], columns=["sentence", "word_labels"])
test_dataset=pd.DataFrame([[" ".join([y[0] for y in x]), ",".join([y[2] for y in x])] for x in limit(test_dataset)], columns=["sentence", "word_labels"])
dev_dataset=pd.DataFrame([[" ".join([y[0] for y in x]), ",".join([y[2] for y in x])] for x in limit(dev_dataset)], columns=["sentence", "word_labels"])



tags_vals=list(set([y[2] for x in data_orig for y in x ]))
labels_to_ids = {t: i for i, t in enumerate(tags_vals)}
ids_to_labels={v:k for k,v in labels_to_ids.items()}

#data=pd.DataFrame(data, columns=["sentence", "word_labels"])



MAX_LEN = 200
TRAIN_BATCH_SIZE = 32
VALID_BATCH_SIZE = 16
EPOCHS = 5
LEARNING_RATE = 1e-05 #5.0e-6   : en general parece mejor poner un LR ligeramente mas grande: 1e-05 y 2e-05 funcionan bien, testar 5e-05 y 1e-04
MAX_GRAD_NORM = 10
tokenizer = BertTokenizerFast.from_pretrained('bert-base-multilingual-cased')




class dataset(Dataset):
  def __init__(self, dataframe, tokenizer, max_len):
        self.len = len(dataframe)
        self.data = dataframe
        self.tokenizer = tokenizer
        self.max_len = max_len

  def __getitem__(self, index):
        # step 1: get the sentence and word labels 
        sentence = self.data.sentence[index].strip().split()  
        word_labels = self.data.word_labels[index].split(",") 

        # step 2: use tokenizer to encode sentence (includes padding/truncation up to max length)
        # BertTokenizerFast provides a handy "return_offsets_mapping" functionality for individual tokens
        encoding = self.tokenizer(sentence,
                             is_pretokenized=True, 
                             return_offsets_mapping=True, 
                             padding='max_length', 
                             truncation=True, 
                             max_length=self.max_len)
        
        # step 3: create token labels only for first word pieces of each tokenized word
        labels = [labels_to_ids[label] for label in word_labels] 
        # code based on https://huggingface.co/transformers/custom_datasets.html#tok-ner
        # create an empty array of -100 of length max_length
        encoded_labels = np.ones(len(encoding["offset_mapping"]), dtype=int) * -100
        
        # set only labels whose first offset position is 0 and the second is not 0
        i = 0
        for idx, mapping in enumerate(encoding["offset_mapping"]):
          if mapping[0] == 0 and mapping[1] != 0:
            # overwrite label
            encoded_labels[idx] = labels[i]
            i += 1

        # step 4: turn everything into PyTorch tensors
        item = {key: torch.as_tensor(val) for key, val in encoding.items()}
        item['labels'] = torch.as_tensor(encoded_labels)
        
        return item

  def __len__(self):
        return self.len


#train_size = 0.9
#train_dataset = data.sample(frac=train_size,random_state=200)
#test_dataset = data.drop(train_dataset.index).reset_index(drop=True)
#train_dataset = train_dataset.reset_index(drop=True)

print("FULL Dataset: {}".format(data.shape))
print("TRAIN Dataset: {}".format(train_dataset.shape))
print("TEST Dataset: {}".format(test_dataset.shape))
print("DEV Dataset: {}".format(dev_dataset.shape))

training_set = dataset(train_dataset, tokenizer, MAX_LEN)
testing_set = dataset(test_dataset, tokenizer, MAX_LEN)
dev_set= dataset(dev_dataset, tokenizer, MAX_LEN)





train_params = {'batch_size': TRAIN_BATCH_SIZE,
                'shuffle': True,
                'num_workers': 0
                }

test_params = {'batch_size': VALID_BATCH_SIZE,
                'shuffle': True,
                'num_workers': 0
                }

training_loader = DataLoader(training_set, **train_params)
testing_loader = DataLoader(testing_set, **test_params)
dev_loader = DataLoader (dev_set, **test_params)






model = BertForTokenClassification.from_pretrained('bert-base-multilingual-cased', num_labels=len(labels_to_ids))
model.to(device)





inputs = training_set[2]
input_ids = inputs["input_ids"].unsqueeze(0)
attention_mask = inputs["attention_mask"].unsqueeze(0)
labels = inputs["labels"].unsqueeze(0)

input_ids = input_ids.to(device)
attention_mask = attention_mask.to(device)
labels = labels.to(device)

outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
initial_loss = outputs[0]
initial_loss





tr_logits = outputs[1]
tr_logits.shape




optimizer = torch.optim.Adam(params=model.parameters(), lr=LEARNING_RATE)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.5, patience=2, verbose=True)






def valid(model, testing_loader):
    # put model in evaluation mode
    model.eval()
    
    eval_loss, eval_accuracy = 0, 0
    nb_eval_examples, nb_eval_steps = 0, 0
    eval_preds, eval_labels = [], []
    
    with torch.no_grad():
        for idx, batch in enumerate(testing_loader):
            
            ids = batch['input_ids'].to(device, dtype = torch.long)
            mask = batch['attention_mask'].to(device, dtype = torch.long)
            labels = batch['labels'].to(device, dtype = torch.long)
            
            loss, eval_logits = model(input_ids=ids, attention_mask=mask, labels=labels)
            
            eval_loss += loss.item()

            nb_eval_steps += 1
            nb_eval_examples += labels.size(0)
        
            if idx % 400==0:
                loss_step = eval_loss/nb_eval_steps
                print(f"Validation loss per 400 evaluation steps: {loss_step}")
              
            # compute evaluation accuracy
            flattened_targets = labels.view(-1) # shape (batch_size * seq_len,)
            active_logits = eval_logits.view(-1, model.num_labels) # shape (batch_size * seq_len, num_labels)
            flattened_predictions = torch.argmax(active_logits, axis=1) # shape (batch_size * seq_len,)
            
            # only compute accuracy at active labels
            active_accuracy = labels.view(-1) != -100 # shape (batch_size, seq_len)
        
            labels = torch.masked_select(flattened_targets, active_accuracy)
            predictions = torch.masked_select(flattened_predictions, active_accuracy)
            
            eval_labels.extend(labels)
            eval_preds.extend(predictions)
            
            tmp_eval_accuracy = accuracy_score(labels.cpu().numpy(), predictions.cpu().numpy())
            eval_accuracy += tmp_eval_accuracy

    labels = [ids_to_labels[id.item()] for id in eval_labels]
    predictions = [ids_to_labels[id.item()] for id in eval_preds]
    
    eval_loss = eval_loss / nb_eval_steps
    eval_accuracy = eval_accuracy / nb_eval_steps
    print(f"Validation Loss: {eval_loss}")
    print(f"Validation Accuracy: {eval_accuracy}")

    return labels, predictions





# Defining the training function on the 80% of the dataset for tuning the bert model
def train(epoch):
    tr_loss, tr_accuracy = 0, 0
    nb_tr_examples, nb_tr_steps = 0, 0
    tr_preds, tr_labels = [], []
    # put model in training mode
    model.train()
    
    for idx, batch in enumerate(training_loader):
        
        ids = batch['input_ids'].to(device, dtype = torch.long)
        mask = batch['attention_mask'].to(device, dtype = torch.long)
        labels = batch['labels'].to(device, dtype = torch.long)

        loss, tr_logits = model(input_ids=ids, attention_mask=mask, labels=labels)
        tr_loss += loss.item()

        nb_tr_steps += 1
        nb_tr_examples += labels.size(0)
        
        if idx % 200==0:
            loss_step = tr_loss/nb_tr_steps
            print(f"Training loss per 200 training steps: {loss_step}")
           
        # compute training accuracy
        flattened_targets = labels.view(-1) # shape (batch_size * seq_len,)
        active_logits = tr_logits.view(-1, model.num_labels) # shape (batch_size * seq_len, num_labels)
        flattened_predictions = torch.argmax(active_logits, axis=1) # shape (batch_size * seq_len,)
        
        # only compute accuracy at active labels
        active_accuracy = labels.view(-1) != -100 # shape (batch_size, seq_len)
        #active_labels = torch.where(active_accuracy, labels.view(-1), torch.tensor(-100).type_as(labels))
        
        labels = torch.masked_select(flattened_targets, active_accuracy)
        predictions = torch.masked_select(flattened_predictions, active_accuracy)
        
        tr_labels.extend(labels)
        tr_preds.extend(predictions)

        tmp_tr_accuracy = accuracy_score(labels.cpu().numpy(), predictions.cpu().numpy())
        tr_accuracy += tmp_tr_accuracy
    
        # gradient clipping
        torch.nn.utils.clip_grad_norm_(
            parameters=model.parameters(), max_norm=MAX_GRAD_NORM
        )
        
        # backward pass
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    epoch_loss = tr_loss / nb_tr_steps
    tr_accuracy = tr_accuracy / nb_tr_steps
    scheduler.step(epoch_loss)



    print(f"Training loss epoch: {epoch_loss}")
    print(f"Training accuracy epoch: {tr_accuracy}")
    labels, predictions = valid(model, dev_loader)














for epoch in range(EPOCHS):
    print(f"Training epoch: {epoch + 1}")
    train(epoch)






labels, predictions = valid(model, testing_loader)


from sklearn import metrics
print(metrics.classification_report(labels, predictions, digits=3, labels=["B-PERS", "I-PERS"]))
result="\n".join(["\t".join([x,y]) for x,y in list(zip(labels, predictions))])
with open('PERS_test_Bert.txt', 'w') as f: f.write(result)

