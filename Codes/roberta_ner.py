
import pandas as pd
import os

dir="/home/magistermilitum/embeddings/new_datasets/"

train_all=[]

with open(dir+'train_set_all_17_03_2022.txt', 'r') as f:
    train = f.read()
    train=train.split("\n")
    train=[x.split("\t", 4) for x in train]
    caja=[]
    for x in train:
      if x==['']:
        train_all.append(caja)
        caja=[]

      else:
        caja.append(x)
    train_all.append(caja)
    
test_all=[]

with open(dir+'test_set_all_17_03_2022.txt', 'r') as f:
    test = f.read()
    test=test.split("\n")
    test=[x.split("\t", 4) for x in test]
    caja=[]
    for x in test:
      if x==['']:
        test_all.append(caja)
        caja=[]

      else:
        caja.append(x)
    test_all.append(caja)
    

val_all=[]

with open(dir+'val_set_all_17_03_2022.txt', 'r') as f:
    val = f.read()
    val=val.split("\n")
    val=[x.split("\t", 4) for x in val]
    caja=[]
    for x in val:
      if x==['']:
        val_all.append(caja)
        caja=[]

      else:
        caja.append(x)
    val_all.append(caja)
    

train_all=[[y if y[0]!="" else ["-"]+y[1:] for y in x] for x in train_all]
test_all=[[y if y[0]!="" else ["-"]+y[1:] for y in x] for x in test_all]
val_all=[[y if y[0]!="" else ["-"]+y[1:] for y in x] for x in val_all]



txt_files=train_all+test_all+val_all

'''
a=[]

for x in txt_files:
    caja=[]
    for y in x:
        if "LOC" in y[2] and "PERS" not in y[1]: #agregar LOC no anidado
            caja.append([y[0], y[2]])
        else:
            caja.append([y[0], y[1]])
            
    a.append(caja)

a=[]
cajon_especial=[]
for x in txt_files:
    caja=[]
    for y in x:
        if "LOC" in y[2] and "PERS" not in y[1]: #agregar LOC no anidado
            caja.append([y[0], y[2]])
        else:
            if "LOC" in y[2] and "PERS" in y[1]:
            #caja.append([y[0], y[2]])
                if "B-PERS" in y[1]:
                  caja.append([y[0], y[1]])
                else:
                  caja.append([y[0], "L-PERS"])
                #cajon_especial.append(y)
            else:
                caja.append([y[0], y[1]])

    a.append(caja)
'''


import codecs 
train_all=txt_files[:len(train_all)]
test_all=txt_files[len(train_all):len(train_all)+len(test_all)]
val_all=txt_files[len(train_all)+len(test_all):]


def limit(dt, num=170):
    data=[]
    box=[]
    counter=0
    nnn=0
    f = lambda A, n=100: [A[i:i+n] for i in range(0, len(A), n)]#si una sent tiene por ejemplo 250 la divide en 3 partes de 100, 100 y 50

    for i,item in enumerate(dt):
      for i_i, word in enumerate(item[:-1]): #el try es porque en algunos docs (esp) el token esta vacio
        try:
          if word[0]=="." and item[i_i+1][0][0].isupper() and counter>12:
            data.extend(f(box+[word]))
            box=[]
            counter=0
          else:
            box.append(word)
            counter+=1
        except:
          nnn+=1
          continue
          

      if len(box)>0:
        if len(box)>num:
          data.extend(f(box))
          counter=0
        else:
          data.append(box)
          counter=0
      data[-1]=data[-1]+[item[-1]]
      box=[]
      counter=0
    print(nnn)
    return data


train_dataset=limit(train_all) #[['Garnerus', 'B-PERS', 'O', 'lat', 'CBMA_18309'], ]
test_dataset=limit(test_all)
dev_dataset=limit(val_all)

#para normal
train_dataset=[[y[:2] for y in x] for x in train_dataset]
test_dataset=[[y[:2] for y in x] for x in test_dataset]
dev_dataset=[[y[:2] for y in x] for x in dev_dataset]


tags_vals=list(set([y[1] for x in train_all for y in x ]))
labels_to_ids = {t: i for i, t in enumerate(tags_vals)}
ids_to_labels={v:k for k,v in labels_to_ids.items()}



TRAIN_FILE_PATH = dir+'roberta/train.txt'
TEST_FILE_PATH = dir+'roberta/test.txt'
EVAL_FILE_PATH  = dir+'roberta/dev.txt'
LABELS_FILE_PATH = dir+'labels.txt'

with open(TRAIN_FILE_PATH,'w') as ftrain:
  for i in train_dataset:
    [ftrain.write(s+' '+t+'\n') for s,t in i]
    ftrain.write('\n')

with open(TEST_FILE_PATH,'w') as ftest:
  for i in test_dataset:
    [ftest.write(s+' '+str(t)+'\n') for s,t in i]
    ftest.write('\n')

with open(EVAL_FILE_PATH,'w') as feval:
  for i in dev_dataset:
    [feval.write(s+' '+str(t)+'\n') for s,t in i]
    feval.write('\n')


with open(LABELS_FILE_PATH,'w') as f:
  for tag in tags_vals:
    print(tag)
    f.write(str(tag)+'\n')


from transformers import AutoTokenizer
tokenizer= AutoTokenizer.from_pretrained('roberta-base')


os.system("python run_ner.py  --model_name_or_path 'xlm-roberta-large' --labels '/home/magistermilitum/embeddings/new_datasets/labels.txt' --data_dir '/home/magistermilitum/embeddings/new_datasets/roberta' \
--output_dir '/home/magistermilitum/embeddings/new_datasets/roberta/model' --max_seq_length '255' --num_train_epochs 3 \
--per_device_train_batch_size 1  --seed 16 --do_train --do_predict --evaluate_during_training --overwrite_output_dir --fp16")
