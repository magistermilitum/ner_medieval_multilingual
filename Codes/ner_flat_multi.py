train_all=[]

with open('train_set_all_10_03_2022', 'r') as f:
    train = f.read()
    train=train.split("\n")
    train=[x.split(" ",4) for x in train]
    caja=[]
    for x in train:
      if x==['']:
        train_all.append(caja)
        caja=[]

      else:
        caja.append(x[0].split("\t"))
    train_all.append(caja)
    
test_all=[]

with open('test_set_all_10_03_2022', 'r') as f:
    test = f.read()
    test=test.split("\n")
    test=[x.split(" ",4) for x in test]
    caja=[]
    for x in test:
      if x==['']:
        test_all.append(caja)
        caja=[]

      else:
        caja.append(x[0].split("\t"))
    test_all.append(caja)
    

val_all=[]

with open('val_set_all_10_03_2022', 'r') as f:
    val = f.read()
    val=val.split("\n")
    val=[x.split(" ",4) for x in val]
    caja=[]
    for x in val:
      if x==['']:
        val_all.append(caja)
        caja=[]

      else:
        caja.append(x[0].split("\t"))
    val_all.append(caja)
    

train_all=[[y if y[0]!="" else ["-"]+y[1:] for y in x] for x in train_all]
test_all=[[y if y[0]!="" else ["-"]+y[1:] for y in x] for x in test_all]
val_all=[[y if y[0]!="" else ["-"]+y[1:] for y in x] for x in val_all]


txt_files=train_all+test_all+val_all
a=[]

for x in txt_files:
    caja=[]
    for y in x:
        if "LOC" in y[2] and "PERS" not in y[1]: #agregar LOC no anidado
            caja.append([y[0], y[2]])
        else:
            caja.append([y[0], y[1]])
            
    a.append(caja)


import codecs 
train_sents=a[:len(train_all)]
test_sents=a[len(train_all):len(train_all)+len(test_all)]
val_sents=a[len(train_all)+len(test_all):]


with codecs.open('train_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in train_sents))

with codecs.open('val_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in val_sents))


with codecs.open('test_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in test_sents))



# define columns to construct our model on flair data
from flair.models import SequenceTagger
from flair.data import Sentence
from flair.data import Corpus
from flair.datasets import ColumnCorpus
columns = {0 : 'text', 1 : 'ner'} #to train on PERS label
#columns = {0 : 'text', 1 : 'ner'} #to train on LOC label

# directory where the data resides
#data_folder = '/content/NER_multi/' 
data_folder = '/content/' 


# initializing the corpus: you must load the training sets in your enviromment
'''
corpus: Corpus = ColumnCorpus(data_folder, columns,
                              train_file = 'train_set_french_08_03_2022.txt',
                              test_file = 'test_set_french_08_03_2022.txt',
                              dev_file = 'val_set_french_08_03_2022.txt')
'''

corpus: Corpus = ColumnCorpus(data_folder, columns,
                              train_file = 'train_elec.txt',
                              test_file = 'test_elec.txt',
                              dev_file = 'val_elec.txt')



# tag to predict
tag_type = 'ner'
# make tag dictionary from the corpus
tag_dictionary = corpus.make_tag_dictionary(tag_type=tag_type)

print(tag_dictionary)



from flair.embeddings import FlairEmbeddings, TransformerWordEmbeddings
from flair.embeddings import WordEmbeddings, StackedEmbeddings


flair_forward_embedding = FlairEmbeddings('multi_19M_forward.pt')
flair_backward_embedding = FlairEmbeddings('multi_19M_backward.pt')

# init multilingual BERT
#bert_embedding = TransformerWordEmbeddings('bert-base-multilingual-cased')

#embeddings = WordEmbeddings('/path/to/converted')

from flair.embeddings import StackedEmbeddings

# now create the StackedEmbedding object that combines all embeddings
'''
stacked_embeddings = StackedEmbeddings(
    embeddings=[WordEmbeddings('/content/gdrive/MyDrive/Colab Notebooks/all_spanish_diplo.bin'),])
'''
stacked_embeddings = StackedEmbeddings(
    embeddings=[flair_forward_embedding, flair_backward_embedding])

#defining architecture to model
from flair.models import SequenceTagger
from flair.embeddings import FlairEmbeddings, BertEmbeddings

# init embeddings from your trained LM
#embeddings = FlairEmbeddings('/content/resources/taggers/language_model/best-lm.pt')


tagger : SequenceTagger = SequenceTagger(hidden_size=256,
                                       embeddings=stacked_embeddings,
                                       tag_dictionary=tag_dictionary,
                                       tag_type=tag_type,
                                       use_crf=True)
print(tagger)


#modeling: this can take several hours depending of your GPU capacity
from flair.trainers import ModelTrainer
trainer : ModelTrainer = ModelTrainer(tagger, corpus)
trainer.train(data_folder,
              learning_rate=0.1,
              mini_batch_size=8,
              max_epochs=50, 
              embeddings_storage_mode='cpu', 
              checkpoint=True)

              