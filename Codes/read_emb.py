
from sklearn.model_selection import train_test_split
import codecs
import json
import pandas as pd


with open('all_multilingual_31_01_2022.json', encoding="utf-8") as f:
  txt_files = json.load(f) #list of list: [['Sancti', 'B-LOC', 'O'], ['Vincentii', 'I-LOC', 'O']]


#txt_files=[[[y[0], y[1]] if "LOC" in y[1] and "PERS" not in y[2] else [y[0], y[2]]] for x in txt_files for y in x]


#codigo para generar flat
a=[]
for x in txt_files:
    caja=[]
    for y in x:
        if "LOC" in y[1] and "PERS" not in y[2]: #agregar LOC no anidado
            caja.append([y[0], y[1]])
        else:
            if "LOC" in y[1] and "PERS" in y[2]:
            #caja.append([y[0], y[2]])
                caja.append([y[0], "L-PERS"])
            else:
                caja.append([y[0], y[2]])

    a.append(caja)

'''
#codigo para BILOU
def bio2bilou(lista, ent, col=1):
    caja=[]
    #list of list
    for i in range(len(lista)):
        if i+1==len(lista):#last token
            if lista[i][col]=="I-"+ent:
                caja.append("L-"+ent)
            elif lista[i][col]=="B-"+ent:
                caja.append("U-"+ent)
            else:
                caja.append("O")
        else:
            if lista[i][col]=="B-"+ent and lista[i+1][col]=="O":
                caja.append("U-"+ent)
            elif lista[i][col]=="B-"+ent and lista[i+1][col]=="I-"+ent:
                caja.append("B-"+ent)
            elif lista[i][col]=="I-"+ent and lista[i+1][col]=="I-"+ent:
                caja.append("I-"+ent)
            elif lista[i][col]=="I-"+ent and lista[i+1][col]=="O":
                caja.append("L-"+ent)
            else:
                caja.append("O")
    return caja #list of annotation

a=[]
for x in txt_files:
    b=[y[0] for y in x]
    c=bio2bilou(x, "PERS", col=2)
    d=[[x,y] for x,y in list(zip(b,c))]
    
    a.append(d)
print(a[10])

'''

#from sklearn.model_selection import train_test_split 
#train, test = train_test_split(X, test_size=0.2, stratify=X['YOUR_COLUMN_LABEL'])

train_sents, test_sents= train_test_split(a, test_size=0.10, random_state=2018)
train_sents, val_sents= train_test_split(train_sents, test_size=0.07, random_state=2018)


with codecs.open('train_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in train_sents))

with codecs.open('val_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in val_sents))


with codecs.open('test_elec.txt', 'w', encoding="utf-8") as f:
    f.write("\n\n".join("\n".join(["\t".join(z) for z in y]) for y in test_sents))


# define columns to construct our model on flair data
from flair.models import SequenceTagger
from flair.data import Sentence
from flair.data import Corpus
from flair.datasets import ColumnCorpus
#columns = {0 : 'text', 2 : 'ner'} #to train on PERS label
columns = {0 : 'text', 1 : 'ner'} #to train on LOC label

# directory where the data resides
data_folder = '' 

# initializing the corpus: you must load the training sets in your enviromment
corpus: Corpus = ColumnCorpus(data_folder, columns,
                              train_file = 'train_elec.txt',
                              test_file = 'test_elec.txt',
                              dev_file = 'val_elec.txt')



# tag to predict
tag_type = 'ner'
# make tag dictionary from the corpus
tag_dictionary = corpus.make_tag_dictionary(tag_type=tag_type)


print(tag_dictionary)


#EMBEDDINGS

from flair.embeddings import FlairEmbeddings, TransformerWordEmbeddings
from flair.embeddings import WordEmbeddings
from flair.data import Sentence
from flair.embeddings import WordEmbeddings, StackedEmbeddings, FlairEmbeddings

#bert_embedding = TransformerWordEmbeddings('bert-base-multilingual-uncased')

flair_forward_embedding = FlairEmbeddings('/home/magistermilitum/embeddings/multi_19M_forward.pt') 
flair_backward_embedding = FlairEmbeddings('/home/magistermilitum/embeddings/multi_19M_backward.pt')

# init multilingual BERT
#bert_embedding = TransformerWordEmbeddings('bert-base-multilingual-cased')



# now create the StackedEmbedding object that combines all embeddings
stacked_embeddings = StackedEmbeddings(
    embeddings=[flair_forward_embedding, flair_backward_embedding])


tagger : SequenceTagger = SequenceTagger(hidden_size=256,
                                       embeddings=stacked_embeddings,
                                       tag_dictionary=tag_dictionary,
                                       tag_type=tag_type,
                                       use_crf=True)
print(tagger)





from flair.trainers import ModelTrainer
trainer : ModelTrainer = ModelTrainer(tagger, corpus)

trainer.train(data_folder,
              learning_rate=0.1,
              mini_batch_size=8,
              max_epochs=50, 
              embeddings_storage_mode='cpu', 
              checkpoint=True)


'''
#code for testing

from flair.models import SequenceTagger

# load the model to evaluate
tagger: SequenceTagger = SequenceTagger.load('/home/magistermilitum/embeddings/LOC_final-model_31_01_2022.pt')


# run evaluation procedure
result= tagger.evaluate(corpus.test, out_path=f"predictions_eslonza_LOC.tsv", gold_label_type='ner', mini_batch_size=8)
print(result.detailed_results)
'''